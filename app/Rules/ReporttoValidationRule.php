<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ReporttoValidationRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value === "Select reportTo..."){
            return false;
        } elseif ($value === "Unassigned"){
            return false;
        }else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The reportto field is required.';
    }
}
