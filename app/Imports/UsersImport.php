<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;

class UsersImport implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'name'      => $row[0],
            'surname'   => $row[1],
            'email'     => $row[2],
            'phone'     => $row[3],
            'work'      => $row[4],
            'jobtitle'  => $row[5],
            'division'  => $row[6],
            'region'    => $row[7],
            'company'   => $row[8],
            'admin'   => 0,
            'password'  => "$2y$10$/e6s4eF5xRdycoXpVYrq8utQlfHtSK7YDFlYg6LoOCVCvh6GLpYFK"
        ]);
    }

    /**
     * Skips the first row
     *
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * specifying a batch size to reduce the import duration
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * Reads the spreadsheet in chunks and keep the memory usage under control.
     */
    public function chunkSize(): int
    {
        return 1000;
    }
}
