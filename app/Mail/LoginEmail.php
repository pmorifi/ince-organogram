<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;

class LoginEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user object instance.
     *
     * @var user
     */
    public $user;

    /**
     * URL.
     *
     * @var string $url
     */
    public $purl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;

        $this->purl =  URL::signedRoute('profile', $user->id);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject(sprintf('Welcome to %s', config('app.name')));
        $this->from('Organogram@example.com');
        $this->markdown('email.login');

        return $this;
    }
}
