<?php

namespace App\Helpers;

class Alert
{
    /** @var string UpdateSuccess */
    const UpdateSuccess = 'updated successfully!';

    /** @var string UpdateFailed */
    const UpdateFailed = 'update failed, please try again!';

    /** @var string StoreSuccess */
    const StoreSuccess = 'stored successfully!';

    /** @var string StoreFailed */
    const StoreFailed = 'storing failed, please try again!';

    /** @var string DestroySuccess */
    const DestroySuccess = 'destroyed successfully!';

    /** @var string DestroyFailed */
    const DestroyFailed = 'destroy failed, please try again!';

    /** @var string TrashSuccess */
    const TrashSuccess = 'trashed successfully!';

    /** @var string TrashFailed */
    const TrashFailed = 'trash failed, please try again!';

    /** @var string RestoreSuccess */
    const RestoreSuccess = 'enabled successfully!';

    /** @var string RestoreFailed */
    const RestoreFailed = 'enable failed, please try again!';

    /** @var string SendSuccess */
    const SendSuccess = 'sent successfully!';

    /** @var string SendFailed */
    const SendFailed = 'send failed, please try again!';

    /** @var string AddSuccess */
    const AddSuccess = 'added successfully!';

    /** @var string AddFailed */
    const AddFailed = 'adding failed, please try again!';

    /** @var string PlaceSuccess */
    const PlaceSuccess = 'placed successfully!';

    /** @var string PlaceFailed */
    const PlaceFailed = 'placing failed, please try again!';

    /** @var string SubscribeSuccess */
    const SubscribeSuccess = 'subscribed successfully!';

    /** @var string SubscribeFailed */
    const SubscribeFailed = 'subscribing failed, please try again!';

    /** @var string UnsubscribeSuccess */
    const UnsubscribeSuccess = 'unsubscribed successfully!';

    /** @var string UnsubscribeFailed */
    const UnsubscribeFailed = 'unsubscribing failed, please try again!';

    /** @var string ApproveSuccess */
    const ApproveSuccess = 'approved successfully!';

    /** @var string ApproveFailed */
    const ApproveFailed = 'approving failed, please try again!';

    /** @var string DenySuccess */
    const DenySuccess = 'denied successfully!';

    /** @var string DenyFailed */
    const DenyFailed = 'denying failed, please try again!';

    /**
     * Show alert according to CRUD results.
     *
     * @param bool $success
     * @param string $action
     * @param string $context
     * @return array
     */
    public static function crud(bool $success, string $action, string $context = null) : array
    {
        $message = self::messageForAction($action, $success);

        $body = $context ? sprintf('%s %s', ucfirst($context), $message) : ucfirst($message);
        $status = $success ? 'success' : 'error';
        $header = $success ? 'Success' : 'Failed';

        return self::custom($body, $status, $header, 1900);
    }

    /**
     * Display success message.
     *
     * @param string $body
     * @param int $timer
     * @return array
     */
    public static function success(string $body, int $timer = 1900) : array
    {
        $alert = [
            'header' => 'Success',
            'body'   => $body,
            'status' => 'success',
            'timer'  => $timer,
        ];

        session()->flash('sweetalert', $alert);

        return $alert;
    }

    /**
     * Display error message.
     *
     * @param string $body
     * @param int $timer
     * @return array
     */
    public static function error(string $body, int $timer = 3100) : array
    {
        $alert = [
            'header' => 'Failed',
            'body'   => $body,
            'status' => 'error',
            'timer'  => $timer,
        ];

        session()->flash('sweetalert', $alert);

        return $alert;
    }

    /**
     * Display warning message.
     *
     * @param string $body
     * @param int $timer
     * @return array
     */
    public static function warning(string $body, int $timer = 2500) : array
    {
        $alert = [
            'header' => 'Warning',
            'body'   => $body,
            'status' => 'warning',
            'timer'  => $timer,
        ];

        session()->flash('sweetalert', $alert);

        return $alert;
    }

    /**
     * Display info message.
     *
     * @param string $body
     * @param int $timer
     * @return array
     */
    public static function info(string $body, int $timer = 2500) : array
    {
        $alert = [
            'header' => 'info',
            'body'   => $body,
            'status' => 'info',
            'timer'  => $timer,
        ];

        session()->flash('sweetalert', $alert);

        return $alert;
    }


    /**
     * Display custom message.
     *
     * @param string $body
     * @param string $status
     * @param string $header
     * @param int $timer
     * @return array
     */
    public static function custom(string $body, string $status = 'warning', string $header = 'Alert!', int $timer = 3000) : array
    {
        $alert = compact('header', 'body', 'status', 'timer');

        session()->flash('sweetalert', $alert);

        return $alert;
    }

    /**
     * Return a message for given action.
     *
     * @param string $action
     * @param bool $success
     * @return string
     */
    private static function messageForAction(string $action, bool $success) : string
    {
        switch ($action) {
            // default
            case 'update' :
                $message = $success ? self::UpdateSuccess : self::UpdateFailed;
                break;
            case 'store' :
                $message = $success ? self::StoreSuccess : self::StoreFailed;
                break;
            case 'destroy' :
                $message = $success ? self::DestroySuccess : self::DestroyFailed;
                break;
            case 'trash' :
                $message = $success ? self::TrashSuccess : self::TrashFailed;
                break;
            case 'restore' :
                $message = $success ? self::RestoreSuccess : self::RestoreFailed;
                break;

            // custom
            case 'send' :
                $message = $success ? self::SendSuccess : self::SendFailed;
                break;
            case 'add' :
                $message = $success ? self::AddSuccess : self::AddFailed;
                break;
            case 'place' :
                $message = $success ? self::PlaceSuccess : self::PlaceFailed;
                break;
            case 'subscribe' :
                $message = $success ? self::SubscribeSuccess : self::SubscribeFailed;
                break;
            case 'unsubscribe' :
                $message = $success ? self::UnsubscribeSuccess : self::UnsubscribeFailed;
                break;
            case 'approve' :
                $message = $success ? self::ApproveSuccess : self::ApproveFailed;
                break;
            case 'deny' :
                $message = $success ? self::DenySuccess : self::DenyFailed;
                break;
            default:
                $message = $success ? 'successful!' : 'failed!';
                break;
        }

        return $message;
    }
}
