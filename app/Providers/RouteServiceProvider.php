<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();

        $this->mapProfileRoutes();

        $this->mapEmailRoutes();

        $this->mapExcelRoutes();

        $this->mapOrganogramRoutes();
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::middleware(['web', 'auth', 'admin'])
            ->name('admin.')
            ->prefix('admin')
            ->namespace('App\Http\Controllers\Admin')
            ->group(base_path('routes/admin.php'));
    }

    /**
     * Define the "profile" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapProfileRoutes()
    {
        Route::middleware(['web', 'auth'])
            ->name('employee.')
            ->prefix('employee')
            ->namespace('App\Http\Controllers\Profile')
            ->group(base_path('routes/profile.php'));
    }

    /**
     * Define the "email" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapEmailRoutes()
    {
        Route::middleware(['web'])
            ->namespace('App\Http\Controllers\Email')
            ->group(base_path('routes/email.php'));
    }

    /**
     * Define the "Organogram" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapOrganogramRoutes()
    {
        Route::middleware(['web'])
            ->namespace('App\Http\Controllers\Organogram')
            ->group(base_path('routes/organogram.php'));
    }

    /**
     * Define the "email" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapExcelRoutes()
    {
        Route::middleware(['web', 'auth', 'admin'])
            ->name('admin.')
            ->prefix('admin')
            ->namespace('App\Http\Controllers\Excel')
            ->group(base_path('routes/excel.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
