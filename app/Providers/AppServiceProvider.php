<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using view composer to set following variables globally
        view()->composer(array('admin.employees.create', 'admin.employees.edit',
            'partials.sidebar', 'partials.navbar'), function($view)
        {
            $view->with('users', User::where('surname', '!=', NULL)->get());

            $view->with('entities', User::where('surname', NULL)->get());
        });
    }
}
