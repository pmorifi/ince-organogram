<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, SoftCascadeTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'division',
        'jobtitle',
        'reportto',
        'phone',
        'region',
        'avatar',
        'work',
        'company',
        'admin',
        'about',
        'assigned',
    ];

    protected $dates = ['deleted_at'];

    protected $softCascade = ['users'];

    /**
     * Deletes and restores on related models using soft deleting.
     *
     * @returns array
     */
    public function users()
    {
        return $this->hasMany('App\User', 'reportto');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Checks if user at Auth is admin or user.
     *
     * @return bool
     */
    public function isAdmin() : bool
    {
        return $this->admin ?? false;
    }

    /**
     * Gets report-To details
     *
     * @returns array
     */
    public function reportTo() {
        return $this->hasOne('App\User', 'id', 'reportto')->withTrashed();
    }
}
