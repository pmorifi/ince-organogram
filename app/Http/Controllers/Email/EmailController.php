<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class EmailController extends Controller
{
    /**
     * Login User from URL.
     *
     * @param int $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(int $user_id)
    {
        $user = User::where('id', $user_id)->firstOrFail();

        Auth::guard('web')->login($user, true);

        return view('profile.login', compact('user'));
    }
}
