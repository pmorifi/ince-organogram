<?php

namespace App\Http\Controllers\Organogram;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Collection;

class OrganogramController extends Controller
{
    /**
     * Show the application organogram.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function organogram($name)
    {
        $employees = User::where('assigned', 1)
            ->where('company', $name)
            ->where('deleted_at', NULL)
            ->get();

        $persons = new Collection();

        foreach ($employees as $employee) {

            $persons->push([
                'id'                    => $employee->id,
                'pid'                   => $employee->reportto,
                'Email'                 => $employee->email,
                'Mobile Number'         => ($employee->work) ? '+27'.ltrim($employee->work, '0') : "",
                'Job Title'             => $employee->jobtitle,
                'Chart Display Name'    => ($employee->work !== NULL) ? '<a href="'.route('employee.profile.show', $employee->id).'" >'.$employee->name.' '.$employee->surname.'</a>' : $employee->name,
                'image'                 => ($employee->avatar) ? $employee->avatar : '/defaultImages/ghost.png'
            ]);
        }

        return view('organogram.home', compact('persons'));
    }
}
