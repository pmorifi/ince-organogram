<?php

namespace App\Http\Controllers\Profile;

use App\Helpers\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProfileRequest;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $show = auth()->user();

        $report = User::where('reportto', $show->id)->get();

        return view('profile.index', compact('show', 'report'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = User::findOrFail($id);

        $report = User::where('reportto', $show->id)->get();

        return view('profile.index', compact('show', 'report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = User::findOrFail($id);

        return view('profile.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CreateProfileRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProfileRequest $request, $id)
    {
        $show = User::findOrFail($id)->update($request->validated());

        Alert::crud($show, 'update');

        return redirect()->route('employee.profile.index');
    }
}
