<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyRequest;
use App\User;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = User::where('surname', NULL)->paginate(10);

        return view('admin.companies.index', compact('entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateCompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompanyRequest $request)
    {
        User::create($request->validated());

        Alert::crud(true, 'store');

        return redirect()->route('admin.companies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = User::findOrFail($id);

        return view('admin.companies.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CreateCompanyRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCompanyRequest $request, $id)
    {
        User::findOrFail($id)->update($request->validated());

        Alert::crud(true, 'update');

        return redirect()->route('admin.companies.index');
    }

    /**
     * Disables the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        Alert::success('Disabled successfully!');

        return redirect()->back();
    }

    /**********************************************************************************************/
    /************************Below are the methods for trashed Companies.**************************/
    /**********************************************************************************************/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewTrashedCompanies()
    {
        $entities = User::where('surname', NULL)
            ->with([
                'reportto' => function($reportto) {  $reportto->withTrashed(); }
            ])
            ->onlyTrashed()
            ->paginate(10);

        return view('admin.companies.trashedcompanies.index', compact('entities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restoreCompany($id)
    {
        $restore = User::withTrashed()->where('id', $id)->firstOrFail();

        $restore->restore();

        Alert::success('Restored successfully.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCompany($id)
    {
        $delete = User::withTrashed()->where('id', $id)->firstOrFail();

        if(file_exists(public_path().$delete->avatar))
        {
            unlink(public_path().$delete->avatar);
        }

        $delete->forceDelete();

        Alert::success('Deleted successfully.');

        return redirect()->back();
    }
}
