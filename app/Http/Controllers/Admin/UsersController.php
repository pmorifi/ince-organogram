<?php

namespace App\Http\Controllers\Admin;

use App\Events\NewUserRegistrationEvent;
use App\Helpers\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUsersRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('surname', '!=', NULL)->paginate(10);

        return view('admin.employees.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUsersRequest $request)
    {
        $user = User::create($request->validated());

        event(new NewUserRegistrationEvent($user));

        Alert::crud(true, 'store');

        return redirect()->route('admin.employees.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = User::findOrFail($id);

        return view('admin.employees.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CreateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateUsersRequest $request, $id)
    {
        if ($request->input('reportto') == $id)
        {
            Alert::warning('User cant report to themselves!');

            return redirect()->back();

        } else {

            User::findOrFail($id)->update($request->validated());

            Alert::crud(true, 'update');

            return redirect()->route('admin.employees.index');
        }
    }

    /**
     * Disables the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->id == $id)
        {
            Alert::warning('You cant disable your account!');

            return redirect()->route('admin.employees.index');

        }else {

            User::findOrFail($id)->delete();

            Alert::success('Disabled successfully!');

            return redirect()->back();
        }
    }

    /**
     * Search for a specific resource from database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchEmployees(Request $request)
    {
        $users = User::where('name', 'like', '%'.$request->search.'%')
            ->orWhere('surname', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->paginate(10);

        if ($users->count() > 0)
        {
            return view('admin.employees.index', compact('users'));
        } else {

            Alert::info('User was not found!.');

            return redirect()->back();
        }
    }

    /**********************************************************************************************/
    /*************************Below are methods for trashed employees.*****************************/
    /**********************************************************************************************/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewTrashedEmployees()
    {
        $users = User::withTrashed()->where('surname', '!=', NULL)->onlyTrashed()->paginate(10);

        return view('admin.employees.trashedemployees.index', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editTrahedEmployee($id)
    {
        $edit = User::withTrashed()->where('id', $id)->firstOrFail();

        return view('admin.employees.edit', compact('edit'));
    }

    /**
     * Re-assign the specified resource in storage.
     *
     * @param  \App\Http\Requests\CreateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reAssignEmployee(CreateUsersRequest $request, $id)
    {
        if ($request->input('reportto') == $id)
        {
            Alert::warning('User cant report to themselves!');

            return redirect()->back();

        } else {

            User::withTrashed()->where('id', $id)->update(['deleted_at' => NULL]);

            User::findOrFail($id)->update($request->validated());

            Alert::success('Reassigned successfully!');

            return redirect()->route('admin.employees.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteEmployee($id)
    {
        $deleteUsers = User::withTrashed()->where('id', $id)->first();

        // Delete avatar
        if(!empty($deleteUsers->avatar))
        {
            $avatar = explode("/", $deleteUsers->avatar);

            Storage::disk('avatar')->delete($avatar[3]);
        }

        $deleteUsers->forceDelete();

        Alert::success('Deleted successfully.');

        return redirect()->back();
    }

    /**
     * Restores a specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restoreEmployee($id)
    {
        $enable = User::withTrashed()->where('id', $id)->firstOrFail();

        if (count(User::withTrashed()
                ->where('id', $enable->reportto)
                ->where('deleted_at', '!=', NULL)->get()) > 0)
        {
            Alert::warning('User has no one to report to.');

            return redirect()->back();
        } else {

            $enable->restore();

            Alert::success('Restored successfully.');

            return redirect()->route('admin.employees.index');
        }
    }

    /**
     * Search for a specific resource from database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchTrashedEmployees(Request $request)
    {
        $users = User::onlyTrashed()->where('name', 'like', '%'.$request->search.'%')
            ->orWhere('surname', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->paginate(10);

        if ($users->count() > 0)
        {
            return view('admin.employees.trashedemployees.index', compact('users'));
        }else {

            Alert::info('User was not found!.');

            return redirect()->back();
        }
    }
}
