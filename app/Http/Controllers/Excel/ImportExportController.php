<?php

namespace App\Http\Controllers\Excel;

use App\Exports\UsersExport;
use App\Helpers\Alert;
use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.importExport.import');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('file'))
        {
            Excel::import(new UsersImport, request()->file('file'));

            Alert::success('Import successful!');

            return redirect()->back();
        }else {

            Alert::error('Import failed!');

            return redirect()->back();
        }
    }

    /**
     * Downloads a listing of the resource.
     */
    public function export()
    {
        $export = Excel::download(new UsersExport, 'users.xlsx');

        return $export;
    }
}
