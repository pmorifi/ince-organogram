<?php

namespace App\Http\Requests;

use App\Rules\DivisionValidationRule;
use App\Rules\PhoneValidationRule;
use App\Rules\RegionValidationRule;
use App\Rules\ReporttoValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class CreateUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->user()->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('PUT')) {
            return $this->updateRules();
        } else {
            return $this->createRules();
        }
    }

    /**
     * Rules for creating resource.
     *
     * @return array
     */
    public function createRules() : array
    {
        return [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'phone' => ['required', 'string', 'numeric', new PhoneValidationRule],
            'work' => ['required', 'string', 'numeric', new PhoneValidationRule],
            'jobtitle' => 'required|string',
            'division' => ['required', 'string', new DivisionValidationRule],
            'region' => ['required', 'string', new RegionValidationRule],
            'reportto' => ['required', 'string', new  ReporttoValidationRule],
            'avatar' => 'nullable|image:jpeg,jpg,png,gif,svg',
            'company' => 'required|string',
            'admin' => 'required|integer',
        ];
    }

    /**
     * Rules for updating resource.
     *
     * @return array
     */
    public function updateRules() : array
    {
        $id = $this->request->get('id');
        return [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($id)],
            'phone' => ['required', 'string', 'numeric', new PhoneValidationRule],
            'work' => ['required', 'string', 'numeric', new PhoneValidationRule],
            'jobtitle' => 'required|string',
            'division' => ['required', 'string', new DivisionValidationRule],
            'region' => ['required', 'string', new RegionValidationRule],
            'reportto' => ['required', 'string', new  ReporttoValidationRule],
            'avatar' => 'nullable|image:jpeg,jpg,png,gif,svg',
            'company' => 'required|string',
            'admin' => 'required|integer',
        ];
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated = $this->getValidatorInstance()->validate();

        $validated['assigned'] = 1;

        // Save avatar
        if ($validated['avatar'] ?? false) {
            $validated['avatar'] = '/storage/avatar/'.self::imageCropper($validated['avatar']);
        }

        return $validated;
    }

    /**
     * Resize and crop image then save.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @return string
     */
    private static function imageCropper(UploadedFile $file) : string
    {
        $file_name = sprintf('%s.%s', sha1(time()), $file->getClientOriginalExtension());

        $img = Image::make($file->path());

        $img->fit(500, null, function($constraint) {
            $constraint->upsize();
        })->crop(500, 500);

        $img->save(storage_path('app/public/avatar/') . $file_name);

        return $file_name;
    }
}
