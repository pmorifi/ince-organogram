<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->user()->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('PUT')) {
            return $this->updateRules();
        } else {
            return $this->createRules();
        }
    }

    /**
     * Rules for creating resource.
     *
     * @return array
     */
    public function createRules() : array
    {
        return [
            'name' => 'required|string',
            'avatar' => 'required|image:jpeg,jpg,png,gif,svg',
        ];
    }

    /**
     * Rules for updating resource.
     *
     * @return array
     */
    public function updateRules() : array
    {
        return [
            'name' => 'required|string',
            'avatar' => 'nullable|image:jpeg,jpg,png,gif,svg'
        ];
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated = $this->getValidatorInstance()->validate();

        $validated['assigned'] = 1;
        $validated['company'] = strtolower($validated['name']);

        // Save avatar
        if ($validated['avatar'] ?? false) {
            $validated['avatar'] = '/defaultImages/'.self::imageCropper($validated['avatar']);
        }

        return $validated;
    }

    /**
     * Resize and crop image then save.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @return string
     */
    private static function imageCropper(UploadedFile $file) : string
    {
        $file_name = sprintf('%s.%s', sha1(time()), $file->getClientOriginalExtension());

        $img = Image::make($file->path());

        $img->fit(500, null, function($constraint) {
            $constraint->upsize();
        })->crop(500, 500);

        $img->save(public_path().'/defaultImages/'. $file_name);

        return $file_name;
    }
}
