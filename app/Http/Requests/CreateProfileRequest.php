<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CreateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('put')) {
            return $this->updateRules();
        }
    }

    /**
     * Rules for updating resource.
     *
     * @return array
     */
    public function updateRules() : array
    {
        return [
            'avatar' => 'nullable|image:jpeg,jpg,png,gif,svg',
            'about' => 'nullable|string|max:150',
            'password' => 'nullable|string'
        ];
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated = $this->getValidatorInstance()->validate();

        // Hash password
        if ($validated['password'] ?? false) {
            $validated['password'] = Hash::make($validated['password']);
        } else {
            unset($validated['password']);
        }

        // Delete/Save avatar
        if ($validated['avatar'] ?? false) {

            if (!empty(auth()->user()->avatar))
            {
                $avatar = explode("/", auth()->user()->avatar);

                Storage::disk('avatar')->delete($avatar[3]);
            }

            $validated['avatar'] = '/storage/avatar/'.self::imageCropper($validated['avatar']);
        }

        return $validated;
    }

    /**
     * Resize and crop image then save.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @return string
     */
    private static function imageCropper(UploadedFile $file) : string
    {
        $file_name = sprintf('%s.%s', sha1(time()), $file->getClientOriginalExtension());

        $img = Image::make($file->path());

        $img->fit(500, null, function($constraint) {
            $constraint->upsize();
        })->crop(500, 500);

        $img->save(storage_path('app/public/avatar/') . $file_name);

        return $file_name;
    }
}
