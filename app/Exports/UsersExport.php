<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class UsersExport implements FromArray, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
     * @return array
     */
    public function array() : array
    {
        $users = User::all();

        $export = [];
        foreach ($users as $user) {
            $export[] = [
                'name'      => $user->name,
                'surname'   => $user->surname,
                'email'     => $user->email,
                'phone'     => $user->phone,
                'work'      => $user->work,
                'jobtitle'  => $user->jobtitle,
                'division'  => $user->division,
                'region'    => $user->region,
                'reportto'  => $user->reportto,
                'company'   => $user->company,
                'admin'     => $user->admin,
            ];
        }
        return $export;
    }

    /**
     * @return array
     */
    public function headings() : array
    {
        return [
            'Name',
            'Surname',
            'Email',
            'Phone',
            'Work',
            'Job Title',
            'Division',
            'Region',
            'Report To',
            'Company',
            'Admin',
        ];
    }

    /**
     * @return array
     */
    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A1:K1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
            },
        ];
    }
}
