<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

use Illuminate\Support\Facades\Route;

// Routes for employees
Route::resource('employees', 'UsersController')->except('show');
Route::prefix('employees')->group(function () {
    // Views
    Route::get('edit/{id}', ['uses' => 'UsersController@editTrahedEmployee', 'as' => 'editTrashedEmployee']);
    Route::get('disabled', ['uses' => 'UsersController@viewTrashedEmployees', 'as' => 'viewTrashedEmployees']);
});

// Routes for companies
Route::resource('companies', 'CompaniesController')->except('show');
Route::prefix('companies')->group(function () {
    Route::get('disabled', ['uses' => 'CompaniesController@viewTrashedCompanies', 'as' => 'viewTrashedCompanies']);
});

// Routes for searching employees
Route::get('search', ['uses' => 'UsersController@searchEmployees', 'as' => 'searchEmployees']);
Route::get('searchTrashedEmployees', ['uses' => 'UsersController@searchTrashedEmployees', 'as' => 'searchTrashedEmployees']);

// Routes for delete, restore and reassign employees
Route::delete('employee/{id}', ['uses' => 'UsersController@deleteEmployee', 'as' => 'deleteEmployee']);
Route::get('restore/employee/{id}', ['uses' => 'UsersController@restoreEmployee', 'as' => 'restoreEmployee']);
Route::put('reassign/employee/{user}', ['uses' => 'UsersController@reAssignEmployee', 'as' => 'reassignEmployee']);

// Routes for delete and restore companies
Route::delete('company/{id}', ['uses' => 'CompaniesController@deleteCompany', 'as' => 'deleteCompany']);
Route::get('companies/edit/{id}', ['uses' => 'CompaniesController@restoreCompany', 'as' => 'restoreCompany']);
