<?php

/*
|--------------------------------------------------------------------------
| Email Routes
|--------------------------------------------------------------------------
*/

use Illuminate\Support\Facades\Route;

// Returns email view
Route::get('/employee/profile/edit/{id}', 'EmailController@login')->where(['id' => '[0-9]+'])->name('profile');
