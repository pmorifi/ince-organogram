<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

use Illuminate\Support\Facades\Route;

// Import
Route::resource('importexport', 'ImportExportController')->except('show', 'destroy', 'update', 'create', 'edit');

// Export
Route::get('export', ['uses' => 'ImportExportController@export', 'as' => 'export']);
