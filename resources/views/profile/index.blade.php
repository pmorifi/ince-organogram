@extends('layouts.master')

@section('content')
    <div class="container-fluid profile">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4 class="font-weight-700">{{(auth()->user()->id === $show->id) ? "My Details" : "User Profile"}}</h4>
                <div class="card card-shadow">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <img class="img img-fluid rounded" src="{{ ($show->avatar) ? asset($show->avatar) : asset('defaultImages/ghost.png') }}" alt="">
                        @if (auth()->user()->id === $show->id)
                            <a href="{{route('employee.profile.edit', $show->id)}}" class="btn btn-primary add-button color-btn"><span><i class="fa fa-edit"></i></span>&nbspEdit&nbspProfile</a>
                        @endif
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Name and Surname</label>
                                    <hr class="mt-0">
                                    {{$show->name}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Name and Surname</label>
                                    <hr class="mt-0">
                                    {{$show->surname}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">E-mail</label>
                                    <hr class="mt-0">
                                    {{$show->email}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Mobile No</label>
                                    <hr class="mt-0">
                                    {{'+27'.ltrim($show->phone, '0')}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Work No</label>
                                    <hr class="mt-0">
                                    {{'+27'.ltrim($show->work, '0')}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Job Title</label>
                                    <hr class="mt-0">
                                    {{$show->jobtitle}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Job Title</label>
                                    <hr class="mt-0">
                                    {{$show->division}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Region</label>
                                    <hr class="mt-0">
                                    {{$show->region}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Report To</label>
                                    <hr class="mt-0">
                                    {{ ($show->reportt !== NULL) ? $show->reportTo->name.' '.$show->reportTo->surename: "Unassigned"}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Company Name</label>
                                    <hr class="mt-0">
                                    {{ ucfirst($show->company)}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">System Role</label>
                                    <hr class="mt-0">
                                    {{ ($show->admin === 1) ? "Admin" : "User"}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold">Bio</label>
                            <hr class="mt-0">
                            {{ $show->about}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold">Assigned to me</label>
                            <hr class="mt-0">
                            @foreach($report as $users)
                                <a class="names" href="{{route('profile.show', $users->id)}}">{{ $users->name.' '.$users->surname.', '}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
@endsection
