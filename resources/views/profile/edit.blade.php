@extends('layouts.master')

@section('content')
    <div class="container-fluid profile">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4 class="font-weight-700">Profile Update</h4>
                <div class="card card-shadow">
                    <div class="card-body">
                        <form method="post" action="{{route('employee.profile.update', $edit->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row justify-content-center">
                                <div class="col-md-4 form-group mb-0">
                                    <div class="form-group">
                                        <div class="img-center">
                                            <img class="img img-fluid rounded-circle profile-picture" id="blah" src="{{asset('defaultImages/ghost.png')}}" /><br>
                                        </div>
                                        <label for="about">Avatar</label>
                                        <hr class="mt-0">
                                        <input type="file" name="avatar"  onchange="readURL(this);" class="form-control @error('avatar') is-invalid @enderror">
                                        @error('avatar')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="about">Bio (About yourself)</label>
                                        <hr class="mt-0">
                                        <textarea name="about" id="text" maxlength="150" class="form-control @error('about') is-invalid @enderror" rows="4">{{$edit->about}}</textarea>
                                        <h6 class="float-right text-muted" id="count_message"></h6>
                                        @error('about')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <hr class="mt-0">
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn-left btn btn-primary color-btn"><span><i class="fa fa-edit"></i></span>Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/custom.js') }}" ></script>
    <script>
        var text_max = 150;
        var counter = 0;
        $('#count_message').html(counter+'/'+text_max);
        $('#text').keyup(function() {
            var text_length = $('#text').val().length;
            var text_remaining = counter + text_length;
            $('#count_message').html(text_remaining+'/'+text_max);
        });
    </script>
@endpush
