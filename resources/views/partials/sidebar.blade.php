<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <div class="sidebar-brand d-flex align-items-center justify-content-center" style="background: white">
        <div class="sidebar-brand-icon rotate-n-15">
            <img src="{{ asset('defaultImages/ince.png') }}" width="55" height="55" class="d-inline-block align-top rounded" alt="">
        </div>
        <div class="sidebar-brand-text mx-3" style="color: #D82F3F;">Organogram</div>
    </div>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        ORGANOGRAM
    </div>

    <li class="nav-item {{ Ekko::isActiveRoute('home.*') }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-fw fa-cog"></i>
            <span>List Of Organogram</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @foreach($entities as $entity)
                    <a class="collapse-item" href="{{route('home', ['name' => $entity->company])}}"><i class="fa fa-link text-danger"></i> <span>{{$entity->name}}</span></a>
                @endforeach
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        MY DETAILS
    </div>
    <li class="nav-item{{ Ekko::isActiveRoute('employee.profile.*') }}">
        <a class="nav-link collapsed" href="{{route('employee.profile.index')}}" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-user"></i>
            <span>Profile</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <li class="nav-item{{ Ekko::isActiveRoute('admin.employees.*') }}">
        <a class="nav-link collapsed" href="{{route('admin.employees.index')}}" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-users"></i>
            <span>Employees</span>
        </a>
    <li class="nav-item{{ Ekko::isActiveRoute('admin.companies.*') }}">
        <a class="nav-link collapsed" href="{{route('admin.companies.index')}}" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-list"></i>
            <span>Companies</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        TRASHED
    </div>

    <li class="nav-item{{ Ekko::isActive('/admin/employees/disabled') }}">
        <a class="nav-link collapsed" href="{{route('admin.viewTrashedEmployees')}}"  data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fa fa-trash"></i>
            <span>Trashed Employees</span>
        </a>
    </li>
    <li class="nav-item{{ Ekko::isActive('/admin/companies/disabled') }}">
        <a class="nav-link collapsed" href="{{route('admin.viewTrashedCompanies')}}"  data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fa fa-trash"></i>
            <span>Trashed Companies</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
        <!-- Heading -->
        <div class="sidebar-heading">
            Excel
        </div>
    <li class="nav-item{{ Ekko::isActive('/admin/importexport') }}">
        <a class="nav-link collapsed" href="{{route('admin.importexport.index')}}"  data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fa fa-database"></i>
            <span>Import & Export Data</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline collapse-button">
        <button class="rounded-circle border-0" id="sidebarToggle" >
            <span></span>
            <span></span>
            <span></span>
        </button>
    </div>
</ul>

@push('scripts')
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarToggle').on('click', function () {
                $(this).toggleClass('active');
            });
        });
    </script>
@endpush
