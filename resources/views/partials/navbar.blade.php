<nav class="navbar navbar-expand topbar static-top shadow">

    @if (auth()->check() && auth()->user()->isAdmin())
        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>
    @endif

    @guest
        <div class="sidebar-brand d-flex align-items-center justify-content-center admin-logo">
            <div class="sidebar-brand-icon rotate-n-15">
                <img src="{{ asset('defaultImages/ince.png') }}" width="55" height="55" class="d-inline-block align-top rounded" alt="">
            </div>
            <div class="sidebar-brand-text orgChart mx-3">Organogram</div>
        </div>
    @endguest
    @if (auth()->check() && !auth()->user()->isAdmin())
        <div class="sidebar-brand d-flex align-items-center justify-content-center" style="background: white">
            <div class="sidebar-brand-icon rotate-n-15">
                <img src="{{ asset('defaultImages/ince.png') }}" width="55" height="55" class="d-inline-block align-top rounded" alt="">
            </div>
            <div class="sidebar-brand-text orgChart mx-3">Organogram</div>
        </div>
    @endif

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="{{asset('defaultImages/orgchart.PNG') }}">
            </a>
            <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Organogram
                </h6>
                @foreach($entities as $entity)
                    <a class="dropdown-item d-flex align-items-center" href="{{route('home', ['name' => $entity->company])}}">
                        <div class="mr-3">
                            <div class="icon-circle">
                                <i class="fa fa-link text-white"></i>
                            </div>
                        </div>
                        <div>
                            <span class="font-weight-bold">{{$entity->name}}</span>
                        </div>
                    </a>
                @endforeach
            </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if (auth()->check())
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{auth()->user()->name.' '.auth()->user()->surname}}</span>
                    <img class="img-profile rounded-circle" src="{{(auth()->user()->avatar) ? auth()->user()->avatar :  asset('defaultImages/ghost.png') }}">
                @else
                    <img class="img-profile rounded-circle" src="{{ asset('defaultImages/Login.PNG') }}">
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                @if (auth()->check())
                    <a class="dropdown-item" href="{{route('employee.profile.index')}}">
                        <i class="fa fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profile
                    </a>
                @endif
                @if (!auth()->check())
                    <a class="dropdown-item" href="{{ route('login') }}">
                        <i class="fa fa-sign-in fa-sm fa-fw mr-2 text-gray-400"></i>
                        {{ __('Login') }}
                    </a>
                @endif
                @if (auth()->check())
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                @endif
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>

    </ul>

</nav>
