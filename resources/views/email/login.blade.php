@component('mail::message')
    # Welcome

    Hi {{ ucwords($user->name) }}

    We hope that all is well,

    You have been registered to Organogram, please click on the button below
    button to update your profile!.

    @component('mail::button', ['url' => $purl])
        Update Profile
    @endcomponent

    Kind regards,<br>
    {{ config('app.name') }}<br>
@endcomponent
