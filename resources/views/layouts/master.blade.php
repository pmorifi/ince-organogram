<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'InceOrganogram') }}</title>
        <link rel="icon" href="{{asset('defaultImages/ince_icon.png')}}">

        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @stack('styles')

    </head>
    <body id="page-top">
        <div id="app">
            <div id="wrapper">

                <!-- Sidebar  -->
                @if (auth()->check() && auth()->user()->isAdmin())
                    @include('partials.sidebar')
                @endif

                <div id="content-wrapper" class="d-flex flex-column">
                    <div id="content">
                        <!-- Navbar  -->
                        @include('partials.navbar')

                        <!-- Page Content  -->
                        <main class="">
                            @yield('content')
                        </main>

                        <!-- SweetAlert  -->
                        @if(session()->has('sweetalert'))<sweet-alert alert="{{ json_encode(session('sweetalert')) }}"></sweet-alert>@endif
                    </div>
                </div>
            </div>
            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        @stack('scripts')

    </body>
</html>
