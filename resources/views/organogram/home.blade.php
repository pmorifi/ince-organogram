@extends('layouts.master')

@section('content')
    <div id="tree"></div>
@endsection

@push('scripts')
    <!-- OrgChart script -->
    <script type="text/javascript" src="{{ asset('getorgchart/orgchart.js') }}"></script>

    <script>
        var data = [];
        let arr = @json($persons)

        function add_array(arry) {
            if (!Array.isArray(arry)) {
                data.push(arry);
            } else {
                for (let i = 0; i < arry.length; i++) {
                    add_array(arry[i]);
                }
            }
        };

        arr.forEach(add_array);

        OrgChart.templates.rony.field_0 = '<text class="field_0" style="font-size: 15px;" fill="white" width="175" x="90" y="30" text-anchor="middle">{val}</text>';
        OrgChart.templates.rony.field_1 = '<text   class="field_1" style="font-size: 14px;" fill="white" width="150" x="90" y="50" text-anchor="middle">{val}</text>';
        OrgChart.templates.rony.field_2 = '<text class="field_2" style="font-size: 14px;" fill="white" width="175" x="90" y="70" text-anchor="middle">{val}</text>';
        OrgChart.templates.rony.field_3 = '<text class="field_3" style="font-size: 14px;" fill="white" x="90" y="90" text-anchor="middle">{val}</text>';

        window.onload = function ()
        {
            var chart = new OrgChart(document.getElementById("tree"),
            {
                template                : "rony",
                toolbar                 : false,
                lazyLoading             : true,
                nodeMouseClickBehaviour : BALKANGraph.action.none,
                showXScroll             : BALKANGraph.scroll.visible,
                showYScroll             : BALKANGraph.scroll.visible,
                layout                  : BALKANGraph.treeLeftOffset,
                scaleInitial            : 0.8,
                oriorientation          : BALKANGraph.orientation.top,
                align                   : BALKANGraph.ORIENTATION,

                nodeBinding: {
                    field_0: "Chart Display Name",
                    field_1: "Job Title",
                    field_2: "Email",
                    field_3: "Mobile Number",
                    img_0  : "image"
                },

                collapse: {
                    level      : 2,
                    allChildren: true
                },

                nodes: data
            });
        };
    </script>
@endpush
