@extends('layouts.master')

@section('content')
    <div class="container-fluid employees">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4 class="font-weight-700">{{($edit->deleted_at !== NULL) ? "Reassign Employee" : "Update Details"}}</h4>
                <div class="card card-shadow">
                    <div class="card-body">
                        <form method="post" action="{{ ($edit->deleted_at == NULL) ? route('admin.employees.update',  ['id' => $edit->id]) : route('admin.reassignEmployee',  ['id' => $edit->id])}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="id" value="{{ $edit->id }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <hr class="devider hrlines">
                                        <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $edit->name }}" placeholder="Name">
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="surname">Surname:</label>
                                        <hr class="devider hrlines">
                                        <input type="text" name="surname" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" value="{{ $edit->surname }}" placeholder="Surname">
                                        @if ($errors->has('surname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('surname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">E-mail</label>
                                        <hr class="devider hrlines">
                                        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ $edit->email }}" placeholder="E-mail">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone">Mobile No:</label>
                                        <hr class="devider hrlines">
                                        <input type="number" name="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ $edit->phone }}" placeholder="Mobile">
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="work">Work No:</label>
                                        <hr class="devider hrlines">
                                        <input type="text" name="work" class="form-control{{ $errors->has('work') ? ' is-invalid' : '' }}" value="{{ $edit->work }}" placeholder="Contact">
                                        @if ($errors->has('work'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('work') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jobtitle">Job Title:</label>
                                        <hr class="devider hrlines">
                                        <input type="text" name="jobtitle" class="form-control{{ $errors->has('jobtitle') ? ' is-invalid' : '' }}" value="{{ $edit->jobtitle }}" placeholder="Job Title">
                                        @if ($errors->has('jobtitle'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('jobtitle') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="division">Division:</label>
                                        <hr class="devider hrlines">
                                        <select name="division" class="form-control @error('division') is-invalid @enderror" >
                                            <option value="{{$edit->name}}" selected>{{$edit->division}}</option>
                                            <option value="IMedia">IMedia</option>
                                            <option value="IBD&M">IBD&M</option>
                                            <option value="Agency">Agency</option>
                                            <option value="Shared Services">Shared Services</option>
                                        </select>
                                        @error('division')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="region">Region:</label>
                                        <hr class="devider hrlines">
                                        <select name="region" class="form-control @error('region') is-invalid @enderror"  >
                                            <option value="{{$edit->region}}" selected>{{$edit->region}}</option>
                                            <option value="KZN">KZN</option>
                                            <option value="Gauteng">Gauteng</option>
                                            <option value="Western Cape">Western Cape</option>
                                        </select>
                                        @error('region')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="reportto">Report To:</label>
                                        <hr class="devider hrlines">
                                        <select name="reportto" class="form-control @error('reportto') is-invalid @enderror">
                                            <option value="{{($edit->reportto !== NULL) ? $edit->reportto : ""}}" selected>
                                                {{($edit->reportto !== NULL) ? $edit->reportTo->name.' '.$edit->reportTo->surname : "Unassigned"}}
                                            </option>
                                            <optgroup label="Company">
                                                @foreach ($entities as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </optgroup>
                                            <optgroup label="Users">
                                                @foreach ($users as $item)
                                                    @if ($item->reportto !== NULL)
                                                        <option value="{{$item->id}}">{{$item->name}} {{$item->surname}}</option>
                                                    @endif
                                                @endforeach
                                            </optgroup>
                                        </select>
                                        @error('reportto')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="avatar">Avatar:</label>
                                        <hr class="devider hrlines">
                                        <input type="file" name="avatar" class="form-control @error('avatar') is-invalid @enderror" placeholder="Avatar">
                                        @error('avatar')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="company">Company Name:</label>
                                        <hr class="devider hrlines">
                                        <select name="company" class="form-control @error('company') is-invalid @enderror" >
                                            <option value="{{$edit->company}}" selected>{{ucfirst($edit->company)}}</option>
                                            @foreach ($entities as $item)
                                                <option value="{{strtolower($item->name)}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('company')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="admin">System Role:</label>
                                        <hr class="devider hrlines">
                                        <select name="admin" class="form-control @error('admin') is-invalid @enderror" >
                                            <option value="{{$edit->admin}}" selected>{{($edit->admin === 0) ? 'User' : 'Admin'}}</option>
                                            <option value="0">User</option>
                                            <option value="1">Admin</option>
                                        </select>
                                        @error('admin')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-at-media color-btn"><span><i class="fa fa-edit"></i></span>{{($edit->deleted_at !== NULL) ? "Reassign": "Update"}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
