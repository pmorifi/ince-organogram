@extends('layouts.master')

@section('content')
    <div class="container-fluid employees">
        <h4 class="font-weight-700">List Of Employees</h4>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-shadow">
                    <div class="card-body">
                        <div class="d-md-flex flex-row">
                            <div class="add-button">
                                <a href="{{route('admin.employees.create')}}" class="btn btn-primary color-btn btn-at-media">
                                    <span><i class="fa fa-user-plus"></i></span></span>
                                    Add Employee
                                </a>
                            </div>
                            <div class="ml-auto create">
                                <form method="get" action="{{route('admin.searchEmployees')}}">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" name="search" class="form-control" placeholder="Search">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-outline-secondary"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered table-sm" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Email</th>
                                    <th>Work&nbspNo</th>
                                    <th>Job&nbspTitle</th>
                                    <th>Division</th>
                                    <th>Report&nbspTo</th>
                                    <th>Region</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($users->count() > 0)
                                    @foreach ($users as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->surname}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>{{'+27'.ltrim($item->work, '0')}}</td>
                                            <td>{{$item->jobtitle}}</td>
                                            <td>{{$item->division}}</td>
                                            @if ($item->reportTo !== NULL)
                                                <td>{{$item->reportTo->name.' '.$item->reportTo->surname}}</td>
                                            @else
                                                <td>Unassigned</td>
                                            @endif
                                            <td>{{$item->region}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div>
                                                        <a href="{{route('employee.profile.show', $item->id)}}" class="btn btn-primary color-btn btn-sm mr-3">
                                                            <i class="fa fa-eye fa-lg"></i>
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <a href="{{route('admin.employees.edit', $item->id)}}" class="btn btn-success btn-sm mr-3">
                                                            <i class=" fa fa-edit fa-lg"></i>
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <form method="post" action="{{route('admin.employees.destroy', $item->id)}}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-sm btn-danger">
                                                                <i class="fa fa-user-times fa-lg"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th colspan="10" class="text-center">No users found</th>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
