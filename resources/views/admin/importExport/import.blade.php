@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card card-shadow">
                    <div class="card-header">Import or Export</div>

                    <div class="card-body">
                        <form action="{{ route('admin.importexport.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control" required>
                            <br>
                            <button class="btn btn-success color-btn">Import User Data</button>
                            <a class="btn btn-success" href="{{route('admin.export')}}">Export User Data</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
