@extends('layouts.master')

@section('content')
    <div class="container-fluid companies">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h4 class="font-weight-700">List Of Trashed Companies</h4>
                <div class="card card-shadow">
                    <div class="card-body">
                        <div class="table-responsive table-button">
                            <table class="table table-hover table-striped table-bordered table-sm" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Logo</th>
                                    <th>Company&nbspName</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($entities->count() > 0)
                                    @foreach ($entities as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>
                                                <img src="{{asset($item->avatar)}}" alt="" class="rounded border border-light logo">
                                            </td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td>{{$item->updated_at}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div>
                                                        <a href="{{route('admin.restoreCompany', ['id' => $item->id])}}" class="btn btn-primary color-btn btn-sm mr-3">
                                                            <i class="fa fa-undo fa-lg"></i>
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <form method="post" action="{{route('admin.deleteCompany', ['id' => $item->id])}}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-sm btn-danger">
                                                                <i class="fa fa-user-times fa-lg"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th colspan="6" class="text-center">No companies found</th>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {{ $entities->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
