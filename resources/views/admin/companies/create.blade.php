@extends('layouts.master')

@section('content')
    <div class="container-fluid companies">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4 class="font-weight-700">Register Company</h4>
                <div class="card card-shadow">
                    <div class="card-body">
                        <form method="post" action="{{route('admin.companies.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-md-3">
                                    <div class="form-group img-center">
                                        <img class="img img-fluid rounded-circle profile-picture" id="blah" src="{{asset('defaultImages/ghost.png')}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="avatar">Company Logo:</label>
                                            <hr class="devider hrlines">
                                            <input type="file" name="avatar" onchange="readURL(this);" class="form-control @error('avatar') is-invalid @enderror">
                                            @error('avatar')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Company Name:</label>
                                            <hr class="devider hrlines">
                                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="Name">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary btn-at-media color-btn"><span><i class="fa fa-user-plus"></i></span>Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/custom.js') }}" ></script>
@endpush
