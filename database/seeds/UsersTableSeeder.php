<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Pule',
            'surname' => 'Morifi',
            'division' => 'iMedia',
            'email' => 'developer@gmail.com',
            'phone' => '0729843218',
            'work' => '0113057300',
            'region' => 'Gauteng',
            'jobtitle' => 'Junior Developer',
            'admin' => 1,
            'company' => 'ince',
            'password' => Hash::make('secret')
        ]);
    }
}
