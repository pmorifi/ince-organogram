<?php

use App\User;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            [
                'name' => 'Ince',
                'avatar' => '/defaultImages/ince.png',
                'assigned' => 1,
                'company' => 'ince'
            ],
            [
                'name' => 'Marble-Tech',
                'avatar' => '/defaultImages/marble.PNG',
                'assigned' => 1,
                'company' => 'marble-tech'
            ],
            [
                'name' => 'Sawubona',
                'avatar' => '/defaultImages/sawubona.PNG',
                'assigned' => 1,
                'company' => 'sawubona'
            ],
            [
                'name' => 'Media-Connect',
                'avatar' => '/defaultImages/media-connect.jpg',
                'assigned' => 1,
                'company' => 'media-connect'
            ]
        ];

        foreach ($companies as $company) {
            User::create($company);
        }
    }
}
